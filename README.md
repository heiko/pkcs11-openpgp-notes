# OpenPGP use of PKCS #&#8203;11 and PIV devices

This project acts as an entry point to work on using different kinds of HSM
for OpenPGP operations, currently focusing on devices using the
PKCS #&#8203;11 or PIV interfaces.

Goals:

- Make widely available hardware security devices usable with OpenPGP.
- Interoperability with other comparable schemes, if possible.
- Make this approach useful for as many scenarios as possible.
- Repeatably derive OpenPGP component keys from smartcard keys
  (that is: the fingerprint of derived keys should be stable for consecutive runs).

*CAUTION: this work is in an early development stage and not yet intended for production use!*

## Overview of subprojects and architecture

```mermaid
graph TB
    OP11T["openpgp-pkcs11-tools <br/> (CLI tool for OpenPGP use of PKCS #11 devices)"] --> OP11S
    SKSP11["Sequoia PKCS #11 keystore driver"] --> OP11S
    P11D["pkcs11-dump <br/> (Low level inspection of PKCS #11 devices)"] --> CRYPTOKI
    OP11S["openpgp-pkcs11-sequoia <br/> (Library for OpenPGP use of PKCS #11 devices)"] --> CRYPTOKI
    OP11S --> OXS
    CRYPTOKI["cryptoki <br/> (Rust-native wrapper around the PKCS #11 API)"]

    OPIVT["openpgp-piv-tools <br/> (CLI tool for OpenPGP use of PIV devices)"] --> OPIVS
    OPIVS["openpgp-piv-sequoia <br/> (Library for OpenPGP use of PIV devices)"] --> YUBIKEY
    OPIVS --> OXS
    YUBIKEY["yubikey.rs <br/> (Pure Rust cross-platform host-side driver [..] <br/> using the Personal Identity Verification (PIV) application)"]

    OXS["openpgp-x509-sequoia <br/> (Shared functionality for handling of X.509 certificates)"]

click OP11T "https://codeberg.org/heiko/openpgp-pkcs11/src/branch/main/cli"
click OP11S "https://codeberg.org/heiko/openpgp-pkcs11/src/branch/main/lib"

click OPIVT "https://codeberg.org/heiko/openpgp-piv/src/branch/main/cli"
click OPIVS "https://codeberg.org/heiko/openpgp-piv/src/branch/main/lib"

click OXS "https://crates.io/crates/openpgp-x509-sequoia"
  
click CRYPTOKI "https://crates.io/crates/cryptoki"
click YUBIKEY "https://crates.io/crates/yubikey"

click P11D "https://codeberg.org/heiko/pkcs11-openpgp-notes/src/branch/main/pkcs11-dump"
```

Subprojects:

#### PKCS #&#8203;11

- CLI tool: [openpgp-pkcs11-tools](https://codeberg.org/heiko/openpgp-pkcs11/src/branch/main/cli)
- Library: [openpgp-pkcs11-sequoia](https://codeberg.org/heiko/openpgp-pkcs11/src/branch/main/lib)

- Sequoia PKCS #&#8203;11 [keystore driver](https://gitlab.com/sequoia-pgp/sequoia-keystore/-/merge_requests/2/)

#### PIV

- CLI tool: [openpgp-piv-tools](https://codeberg.org/heiko/openpgp-piv/src/branch/main/cli)
- Library: [openpgp-piv-sequoia](https://codeberg.org/heiko/openpgp-piv/src/branch/main/lib)

#### X.509 certificate handling

- [openpgp-x509-sequoia](https://codeberg.org/heiko/openpgp-x509-sequoia)

#### CI infrastructure

Additionally, a [suite of PIV card emulators](https://gitlab.com/hkos/virtual-piv)
for convenient use in CI has been developed, and is used for end-to-end tests
in the `openpgp-piv-tools` subproject.

#### Notes

Each of the above repositories contains documentation
(and information about CI testing, where applicable).

Finally, this repository contains high level notes about the intersections of
PKCS #&#8203;11, PIV and OpenPGP.

### End-to-end tests

A major goal of this work is interoperability with pre-existing approaches
to using HSMs for OpenPGP operations.
Thus, we focus on developing end-to-end tests that can run in public CI.

Currently, two test suites exist to ascertain interoperability with
pre-existing approaches:

#### PKCS #&#8203;11 interop with gnupg-pkcs11-scd

  - Setting up and OpenPGP environment with `gnupg-pkcs11-scd`, backed by SoftHSM2 keys (xxx link).
  - Re-use this PKCS #&#8203;11 device to do OpenPGP operations with our Sequoia-based tooling.

#### PIV interop with GnuPG's gpgsm

  - Setting up a GnuPG based environment based on a PIV device (using the `gpgsm` and `gpg-card` tools)
  - Re-use this PIV device to do OpenPGP operations with our Sequoia-based tooling.


# Workflows for OpenPGP / PKCS #&#8203;11 interaction

There are two distinct workflows for using HSMs for OpenPGP operations:

## Key material originates on the PKCS #&#8203;11 side

In this case, the PKCS #&#8203;11 device probably does not contain OpenPGP metadata.

For such setups, some relevant OpenPGP metadata for private key operations can
only be sourced from the user's OpenPGP certificate (aka the OpenPGP public
key).

Both `gnupg-pkcs11-scd` (PKCS #&#8203;11) and `gpgsm` (PIV) use this type of
workflow. 

## Key material originates on the OpenPGP side

This type of workflow can allow embedding OpenPGP metadata on the PKCS #&#8203;11 side (e.g. in the CKO_CERTIFICATE object).

Having OpenPGP metadata available enables reproducible mapping of PKCS #&#8203;11 keys to OpenPGP subkeys
(that is, the fingerprint will be reliably unchanged when importing keys from a card).

### OpenPGP metadata in generated X.509 certificates

*NOTE: Generation of X.509 certificates that encode OpenPGP metadata is
currently an experimental feature*

When uploading OpenPGP material to an HSM, this tool generates X.509
certificates to upload to the card that accompanies the private key material.

In these certificates, additional OpenPGP metadata is stored in the following ways:

- The creation time of the OpenPGP component key is stored in the "validity.not_before" field of the certificate.

- The X.509 "serial number" field is set to the OpenPGP V4 fingerprint by this tool.
  ("The CA can choose the serial number in any way as it sees fit, not necessarily randomly (and it has to fit in 20 bytes).")

- The "Primary User ID" is used in the X.509 "common name" field (the certificate is self-signed,
  so the same common name is also shown as the "Issuer" of the certificate).

#### Additional metadata in X.509 Extensions

A separate API in the `openpgp-x509-sequoia` crate sets two additional pieces
of metadata as X.509 Extensions. This is currently implemented for testing
purposes only, and not exposed in the CLI tools.

- An X.509 Extension is set to contain a representation of the OpenPGP
  component key fingerprint:

```
    Extension {
        id: 1.3.6.1.4.1.11591.2.4.1.2,
        critical: None,
        value: 44364533313832373243414641453136313346313746423745304334324531324146333533374145,
    },
```

- For ECDH decryption keys, an additional X.509 Extension is set to specify KDF/KEK parameters:

```
    Extension {
        id: 1.3.6.1.4.1.11591.2.2.10,
        critical: None,
        value: 03010908,
    },
```

#### OIDs

See
[`gnupg/doc/DETAILS`](https://git.gnupg.org/cgi-bin/gitweb.cgi?p=gnupg.git;a=blob;f=doc/DETAILS;h=a3fe802a218df2e66abc79d830fb7c4aabda1a97;hb=HEAD#l1604)
for "OIDs below the GnuPG arc".


# Existing projects on the intersection of OpenPGP and PKCS #&#8203;11 or PIV

## General discussion

- ["PKCS #11 and GnuPG,
Posted Aug 11, 2011 8:38 UTC (Thu) by dd9jn"](https://lwn.net/Articles/454619/)
  - points to: https://p11-glue.github.io/p11-glue/

- ["Creating a new X.509 certificate from your PGP key pair"](http://wiki.cacert.org/ConvertingPgpKeyToCertificate)

## Relevant tools in the GnuPG suite

```mermaid
graph TB
PKCS11["PKCS #11 <br/> (Mozilla, ...)"] --> SCUTE
SCUTE["scute <br/> 'PKCS #11 provider on top of GnuPG'"] --> SCD
GPGSM["gpgsm <br/> 'CMS encryption and signing tool'"] --> SCD
GPGC["gpg-card <br/> 'Administrate smart cards and USB tokens'  <br/> (alternative to 'gpg --card-edit')"] --> SCD
SCD["scdaemon <br/> - OpenPGP card: scd/app-openpgp.c <br/> - PIV: scd/app-piv.c"]

click SCUTE "https://gnupg.org/documentation/manuals/scute.pdf"
click GPGC "https://gnupg.org/documentation/manuals/gnupg/gpg_002dcard.html#gpg_002dcard"
click GPGSM "https://gnupg.org/documentation/manuals/gnupg/#toc-Invoking-GPGSM-1"
```

### scute

[Scute](https://gnupg.org/software/scute/)
is a PKCS #&#8203;11 provider on top of GnuPG allowing the use of GnuPG supported smartcards with Mozilla's Network Security
Services and other software supporting the PKCS #&#8203;11 specification.

- https://dev.gnupg.org/T6218 (discussion of scute usage)
  
- has been tested against https://www-archive.mozilla.org/projects/security/pki/pkcs11/
  ([see here](https://gnupg.org/documentation/manuals/scute/Developing-Scute.html#Developing-Scute))

[Usage](https://www.gnupg.org/documentation/manuals/scute/Application-Configuration.html#Application-Configuration):
After setting up a card via `gpgsm`, the  `scute.so` library can be configured
in Firefox as a new device "Settings -> Privacy & Security -> Security Devices -> Load".
Certificates from the card is then visible in Firefox via "Settings -> Privacy & Security -> View Certificates".

### gpgsm

[Described](https://www.gnupg.org/documentation/manuals/gnupg/Invoking-GPGSM.html) as follows:
"gpgsm is a tool similar to gpg to provide digital encryption and signing services on X.509 certificates
and the [CMS](https://en.m.wikipedia.org/wiki/Cryptographic_Message_Syntax) protocol.
It is mainly used as a backend for S/MIME mail processing."

- Documentation of example usage: https://gnupg.org/documentation/manuals/gnupg/gpg_002dcard.html

#### Example run against an OpenPGP card

The card has been populated with rsa2k keys:

```
$ gpgsm --gen-key > /tmp/foo.crt
  > 3
  > 3
  > 2
  > CN="Foo Bar",OU="Sales",O="ACME Inc, Ltd",L="Looney Town",ST="Desert",C=RR
  > foo@example.org
  >
  >
  >
  > y
  > y
```

Inspect cert: `$ openssl x509 -in /tmp/foo.crt -text`

`$ gpgsm --import /tmp/foo.crt`

To sign with the key on the card:

First add a line for the key to `$GNUPGHOME/trustlist.txt`, with the value of the `sha1 fpr` field in
`gpgsm --list-keys` (and restart `gpg-agent`).

Then generate a signature (pinentry will ask for the User PIN):

```
$ echo foo | gpgsm -s --armor
gpgsm: signature created
-----BEGIN SIGNED MESSAGE-----
MIAGCSqGSIb3DQEHAqCAMIACAQExDzANBglghkgBZQMEAgEFADCABgkqhkiG9w0B
BwGggCSABARmb28KAAAAAAAAMYICUDCCAkwCAQEwgY4wgYExCzAJBgNVBAYTAlhZ
MRUwEwYDVQQIEwxTbmFrZSBEZXNlcnQxEzARBgNVBAcTClNuYWtlIFRvd24xFzAV
BgNVBAoTDlNuYWtlIE9pbCwgTHRkMRcwFQYDVQQLEw5XZWJzZXJ2ZXIgVGVhbTEU
MBIGA1UEAxMLRmxvcHB5IEhlYWQCCBqtiFmNj+VWMA0GCWCGSAFlAwQCAQUAoIGT
MBgGCSqGSIb3DQEJAzELBgkqhkiG9w0BBwEwHAYJKoZIhvcNAQkFMQ8XDTIyMTIy
OTE2MDIzNlowKAYJKoZIhvcNAQkPMRswGTALBglghkgBZQMEAQIwCgYIKoZIhvcN
AwcwLwYJKoZIhvcNAQkEMSIEILW7nYAUoPmx1h4h55bXjczfE1LyPNMoEvSFC4eK
5JRMMA0GCSqGSIb3DQEBAQUABIIBAJ4NrdDmOIea75R2pXWsei/gR1+y2AcbgXzz
iebzUyx7WRGD4IVDyXvruCqjnl14YzxEaDi0rTyG3bODdROSy8sRnOUsP0QkT05A
n4ycOuJv+ttQOFejUwCG5fkHb68bX9J2D/gcRcmzweO2u3RvqfjnHR7KJCkMS29d
+n1CkR0DH9H4njJy0dpqtdOcI6WRS0pkvU88frV5cKhMnORANZd27cnp66F/fHer
VGGhc7JjpAeA2h3oAQf5zCyjqFhEhqzla41yS3JVmERBIcJVHAcNnXhm5FHgj/rZ
1SXeV5E7RUrjkL2dDpdrWIlzDtv1DytO3UU52pq5IruWWAG7ssoAAAAAAAA=
-----END SIGNED MESSAGE-----
```

## Other projects that interact with GnuPG

### gnupg-pkcs11-scd

"GnuPG-compatible smart-card daemon with PKCS #&#8203;11 support"

https://github.com/alonbl/gnupg-pkcs11-scd
([old sourceforge repo](https://gnupg-pkcs11.sourceforge.net/))

- Setup overview: https://sztsian.github.io/2022/02/20/Using-PKCS11-Token-With-GPG.html
  - Details in manpage: https://manpages.debian.org/bullseye/gnupg-pkcs11-scd/gnupg-pkcs11-scd.1.en.html#GNUPG_INTEGRATION
- Requirements for certs: https://github.com/alonbl/gnupg-pkcs11-scd/issues/46

### piv-agent

https://github.com/smlx/piv-agent

"piv-agent is an SSH and GPG agent providing simple integration of PIV hardware (e.g. a Yubikey) with ssh, and gpg
workflows such as git signing, pass encryption, or keybase chat."

### scd-pkcs11

https://github.com/sektioneins/scd-pkcs11

## Projects outside the GnuPG ecosystem

### wiktor/pkcs11-openpgp

https://gitlab.com/wiktor/pkcs11-openpgp/

- https://github.com/alonbl/gnupg-pkcs11-scd/issues/17#issuecomment-852804571


# Misc projects/pointers to investigate

https://github.com/opencryptoki/opencryptoki

https://github.com/Mastercard/pkcs11-tools

https://gpkcs11.sourceforge.net/

- Peter Gutmann, cryptlib
  - https://lists.gnupg.org/pipermail/gnupg-users/2004-November/023712.html
  - https://www.cs.auckland.ac.nz/~pgut001/cryptlib/index.html
  - https://cryptlib-release.s3-ap-southeast-1.amazonaws.com/manual.pdf

## OpenSSL/PKCS #&#8203;11

PKCS #&#8203;11 engine plugin for the OpenSSL library allows accessing PKCS #&#8203;11 modules in a semi-transparent way:
https://github.com/OpenSC/libp11

- https://colinpaice.blog/2021/03/08/using-openssl-with-an-hsm-keystore-and-opensc-pkcs11-engines/
- https://developers.yubico.com/YubiHSM2/Usage_Guides/OpenSSL_with_pkcs11_engine.html

## IsoApplet

https://github.com/philipWendland/IsoApplet


# Generation of OpenPGP test keys

For easy OpenPGP *test key* generation, the `janus_gen` tool is used in examples:

```
$ cargo run --bin janus_gen nist-p384 >/tmp/janus.pgp
```

The tool supports the following set of algorithms:
`rsa2048`, `rsa3072`, `rsa4096`, `nist-p256`, `nist-p384`, `nist-p521`, `cv25519`.
Generated keys have one User ID "`Janus <janus@example.org>`" and three subkeys
(for encryption, signing and authentication, respectively):

```shell
$ sq inspect /tmp/janus.pgp
/tmp/janus.pgp: Transferable Secret Key.

    Fingerprint: A8DC795DD4D8DCF8FE5BBCE5C4D378E592FE7AC3
Public-key algo: ECDSA
Public-key size: 384 bits
     Secret key: Unencrypted
  Creation time: 2023-02-21 18:44:54 UTC
      Key flags: certification

         Subkey: D6E318272CAFAE1613F17FB7E0C42E12AF3537AE
Public-key algo: ECDSA
Public-key size: 384 bits
     Secret key: Unencrypted
  Creation time: 2023-02-21 18:44:54 UTC
      Key flags: signing

         Subkey: E5829B04AF6B297361A3D307BCA6406152770D70
Public-key algo: ECDSA
Public-key size: 384 bits
     Secret key: Unencrypted
  Creation time: 2023-02-21 18:44:54 UTC
      Key flags: authentication

         Subkey: 7B3BB1F21BD227BEDC7A24FA503C026C844C238A
Public-key algo: ECDH
Public-key size: 384 bits
     Secret key: Unencrypted
  Creation time: 2023-02-21 18:44:54 UTC
      Key flags: transport encryption, data-at-rest encryption

         UserID: Janus <janus@example.org>
```
