# A simple tool for PKCS #&#8203;11 device inspection

## Inspecting HSM content with pkcs11dump

This tool prints a relatively raw representation of the content of a PKCS #&#8203;11 device.

Set `MODULE` (e.g. `export MODULE="/usr/lib64/pkcs11/yubihsm_pkcs11.so"`), then:

```
$ cargo run --bin pkcs11dump
```

## YubiHSM

If the HSM requires login (like e.g. the YubiHSM), also set `PIN` in the environment:

```
$ export PIN=0001password
$ cargo run --bin pkcs11dump
```

(Note that the YubiHSM PKCS #&#8203;11 driver also requires `YUBIHSM_PKCS11_CONF` to
point to a configuration file, see below.)
