use std::borrow::Cow;
use std::env;

use ascii_tree::{write_tree, Tree};
use base64::Engine;
use cryptoki::context::{CInitializeArgs, Pkcs11};
use cryptoki::object::{Attribute, AttributeType};
use cryptoki::session::UserType;
use x509_parser::oid_registry;

lazy_static::lazy_static! {
    static ref OID_REG: oid_registry::OidRegistry<'static> = oid_registry::OidRegistry::default()
        .with_all_crypto()
        .with_x509();
}

/// Print information from connected pkcs#11 devices.
///
/// Set module in environment:
///
/// export MODULE="/usr/lib64/softhsm/libsofthsm.so"
/// export MODULE="/usr/lib64/opensc-pkcs11.so"
/// export MODULE="/usr/lib64/libykcs11.so"
///
/// optionally: set User PIN in environment:
/// export PIN="user"
fn main() -> Result<(), Box<dyn std::error::Error>> {
    let module = env::var("MODULE").expect("MODULE is unset in environment");
    let pin = env::var("PIN");

    let mut pkcs11 = Pkcs11::new(module)?;
    pkcs11.initialize(CInitializeArgs::OsThreads)?;

    for slot in pkcs11.get_slots_with_token()? {
        let mut nodes: Vec<Tree> = vec![]; // nodes

        if let Ok(info) = pkcs11.get_token_info(slot) {
            nodes.push(Tree::Leaf(
                format!("{info:#?}")
                    .lines()
                    .map(|s| s.to_string())
                    .collect(),
            ));

            // open a session
            if let Ok(session) = pkcs11.open_ro_session(slot) {
                // log in the session
                if let Ok(pin) = &pin {
                    session.login(UserType::User, Some(pin))?;
                }

                let obj = session.find_objects(&[])?;
                // let obj = session.find_objects(&vec![Attribute::Token(true)])?;

                let mut objs = vec![];

                for object in obj {
                    let mut attrs = session.get_attributes(
                        object,
                        &[
                            AttributeType::Id,
                            AttributeType::Label,
                            AttributeType::Application,
                            AttributeType::Issuer,
                            AttributeType::Subject,
                            AttributeType::ObjectId,
                            AttributeType::Class,
                            AttributeType::KeyType,
                            //
                            AttributeType::StartDate,
                            AttributeType::EndDate,
                            AttributeType::SerialNumber,
                            // -- RSA
                            AttributeType::ModulusBits,
                            AttributeType::Modulus,
                            AttributeType::PublicExponent,
                            // -- ECC
                            AttributeType::EcParams,
                            AttributeType::EcPoint,
                            // --
                            AttributeType::AcIssuer,
                            AttributeType::AllowedMechanisms,
                            AttributeType::AttrTypes,
                            AttributeType::Base,
                            // AttributeType::CertificateType, // ykcs doesn't like this type
                            AttributeType::CheckValue,
                            AttributeType::Coefficient,
                            AttributeType::Derive,
                            AttributeType::Exponent1,
                            AttributeType::Exponent2,
                            AttributeType::HashOfIssuerPublicKey,
                            AttributeType::HashOfSubjectPublicKey,
                            // AttributeType::KeyGenMechanism, // pivapplet crashes on this, for some objects
                            AttributeType::Owner,
                            AttributeType::Prime,
                            AttributeType::Prime1,
                            AttributeType::Prime2,
                            AttributeType::PrivateExponent,
                            AttributeType::PublicKeyInfo,
                            AttributeType::Url,
                            AttributeType::ValueLen,
                            // -- bools
                            AttributeType::AlwaysAuthenticate,
                            AttributeType::AlwaysSensitive,
                            AttributeType::Copyable,
                            AttributeType::Decrypt,
                            AttributeType::Destroyable,
                            AttributeType::Encrypt,
                            AttributeType::Extractable,
                            AttributeType::Local,
                            AttributeType::Modifiable,
                            AttributeType::NeverExtractable,
                            AttributeType::Private,
                            AttributeType::Sensitive,
                            AttributeType::Sign,
                            AttributeType::SignRecover,
                            AttributeType::Token,
                            AttributeType::Trusted,
                            AttributeType::Unwrap,
                            AttributeType::Verify,
                            AttributeType::VerifyRecover,
                            AttributeType::Wrap,
                            AttributeType::WrapWithTrusted,
                            // --
                            // HACK: asking for value in PivApplet crashes for some objects (?).
                            // HACK: Query of "Value" is moved down, see below.
                            // AttributeType::Value,
                        ],
                    )?;

                    // Asking for "Value" in PivApplet crashes for some objects (why?).
                    // Query the "Value" attribute separately, and add it to 'attrs' if Ok()
                    if let Ok(val) = session.get_attributes(object, &[AttributeType::Value]) {
                        for a in val {
                            attrs.push(a)
                        }
                    }

                    objs.push((object, attrs));
                }

                fn find_id(attrs: &[Attribute]) -> Option<u16> {
                    for a in attrs {
                        if let Attribute::Id(x) = a {
                            match x.len() {
                                1 => return Some(x[0] as u16),
                                2 => return Some(x[0] as u16 * 256 + x[1] as u16),
                                _ => panic!("Unexpected Id {:?}", x),
                            }
                        }
                    }
                    None
                }

                objs.sort_by(|a, b| find_id(&a.1).unwrap().cmp(&find_id(&b.1).unwrap()));

                for (o, a) in objs {
                    let mut vals: Vec<Tree> = vec![];
                    for attr in &a {
                        if let Some(t) = attr_to_tree(attr) {
                            vals.push(t);
                        }
                    }

                    nodes.push(Tree::Node(
                        format!("Object {o:x}")
                            .lines()
                            .map(|s| s.to_string())
                            .collect(),
                        vals,
                    ));
                }
            } else {
                nodes.push(Tree::Leaf(vec![String::from(
                    "[Couldn't open_ro_session for this slot]",
                )]));
            }
        } else {
            nodes.push(Tree::Leaf(vec![String::from("[Error in TokenInfo]")]));
        }

        let tree = Tree::Node(format!("{slot:?}"), nodes);

        let mut output = String::new();
        let _ = write_tree(&mut output, &tree);
        println!("{output}");
    }

    Ok(())
}

/// Format an `Attribute` into a `Tree`
fn attr_to_tree(attr: &Attribute) -> Option<Tree> {
    let s: String = match attr {
        Attribute::Id(id) => format!("ID: {id:x?}"),
        Attribute::Label(str) => format!("Label: {}", String::from_utf8_lossy(str)),
        Attribute::Value(bytes) => {
            if let Ok(cert) = x509_certificate::X509Certificate::from_der(bytes) {
                format!("Value: {cert:#?}")
            } else {
                format!(
                    "Value: {}",
                    base64::engine::general_purpose::STANDARD.encode(bytes)
                )
            }
        }
        Attribute::Issuer(bytes) => {
            // FIXME: this field doesn't just contain a string?
            format!("Issuer: {}", String::from_utf8_lossy(bytes))
        }
        Attribute::Subject(bytes) => {
            // FIXME: this field doesn't just contain a string?
            format!("Subject: {}", String::from_utf8_lossy(bytes))
        }
        Attribute::Application(bytes) => {
            format!("Application: {}", String::from_utf8_lossy(bytes))
        }

        Attribute::Modulus(bytes) => {
            format!(
                "Modulus: {}",
                bytes
                    .iter()
                    .map(|b| format!("{b:02x}"))
                    .collect::<Vec<_>>()
                    .join(" ")
            )
        }
        Attribute::ModulusBits(bits) => format!("ModulusBits: {bits}"),
        Attribute::ObjectId(bytes) => {
            let oid = asn1_rs::Oid::new(Cow::from(bytes));

            if let Some(obj_name) = OID_REG.get(&oid) {
                format!("ObjectID: {oid} [{obj_name:?}]")
            } else {
                format!("ObjectID: {oid}")
            }
        }
        Attribute::KeyType(t) => format!("KeyType: {t}"),
        Attribute::Class(c) => format!("Class: {c}"),

        Attribute::AlwaysAuthenticate(b) => match *b {
            true => "AlwaysAuthenticate".to_string(),
            false => return None,
        },
        Attribute::AlwaysSensitive(b) => match *b {
            true => "AlwaysSensitive".to_string(),
            false => return None,
        },
        Attribute::Copyable(b) => match *b {
            true => "Copyable".to_string(),
            false => return None,
        },
        Attribute::Decrypt(b) => match *b {
            true => "Decrypt".to_string(),
            false => return None,
        },
        Attribute::Derive(b) => match *b {
            true => "Derive".to_string(),
            false => return None,
        },
        Attribute::Destroyable(b) => match *b {
            true => "Destroyable".to_string(),
            false => return None,
        },
        Attribute::Encrypt(b) => match *b {
            true => "Encrypt".to_string(),
            false => return None,
        },
        Attribute::Extractable(b) => match *b {
            true => "Extractable".to_string(),
            false => return None,
        },
        Attribute::Local(b) => match *b {
            true => "Local".to_string(),
            false => return None,
        },
        Attribute::Modifiable(b) => match *b {
            true => "Modifiable".to_string(),
            false => return None,
        },
        Attribute::NeverExtractable(b) => match *b {
            true => "NeverExtractable".to_string(),
            false => return None,
        },
        Attribute::Private(b) => match *b {
            true => "Private".to_string(),
            false => return None,
        },
        Attribute::Sensitive(b) => match *b {
            true => "Sensitive".to_string(),
            false => return None,
        },
        Attribute::Sign(b) => match *b {
            true => "Sign".to_string(),
            false => return None,
        },
        Attribute::SignRecover(b) => match *b {
            true => "SignRecover".to_string(),
            false => return None,
        },
        Attribute::Token(b) => match *b {
            true => "Token".to_string(),
            false => return None,
        },
        Attribute::Trusted(b) => match *b {
            true => "Trusted".to_string(),
            false => return None,
        },
        Attribute::Unwrap(b) => match *b {
            true => "Unwrap".to_string(),
            false => return None,
        },
        Attribute::Verify(b) => match *b {
            true => "Verify".to_string(),
            false => return None,
        },
        Attribute::VerifyRecover(b) => match *b {
            true => "VerifyRecover".to_string(),
            false => return None,
        },
        Attribute::WrapWithTrusted(b) => match *b {
            true => "WrapWithTrusted".to_string(),
            false => return None,
        },
        Attribute::Wrap(b) => match *b {
            true => "Wrap".to_string(),
            false => return None,
        },

        Attribute::Url(bytes) => {
            if !bytes.is_empty() {
                format!("Application: {}", String::from_utf8_lossy(bytes))
            } else {
                return None;
            }
        }

        generic => format!("{generic:02x?}"),
    };

    Some(Tree::Leaf(s.lines().map(|s| s.to_string()).collect()))
}
